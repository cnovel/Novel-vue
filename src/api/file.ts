import http from '@/utils/http';

/**
 * 文件上传
 * @param formData
 */
export const fileUpload = (formData: any) => {
  return http.post('resources/upload', formData, {
    transformRequest: [(params) => {
      return params;
    }],
  });
}
