import http from '@/utils/http';

/**
 * 获取在线用户列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getOnlineList = (params?: any) => {
  return http.get('monitor/online/list', {params});
}

/**
 * 强制退出用户
 * @param sessionId
 */
export const forceLogout = (sessionId: string) => {
  return http.delete('monitor/online/' + sessionId);
}
