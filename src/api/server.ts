import http from '@/utils/http';

/**
 * 获取系统运行信息
 * @returns {*|Promise<any>}
 */
export const getServerInfo = () => {
  return http.get('monitor/server');
}
/**
 * 获取nginx运行信息
 */
export const getNginxInfo = () => {
  return http.get('monitor/server/nginx');
}
