import http from '@/utils/http';

/**
 * 获取系统角色列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getRoleList = (params?: any) => {
  return http.get('system/role/list', {params});
}

/**
 * 获取角色权限树
 * @param params
 * @returns {*|Promise<any>}
 */
export const getRolePermissionTree = (params?: any) => {
  return http.get('system/role/roleMenuTreeData', {params});
}


/**
 * 删除角色信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const removeRole = (params: any) => {
  return http.delete('system/role/remove', {params});
}

/**
 * 保存修改角色信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const updateRole = (params: any) => {
  return http.put('system/role/edit', params);
}

/**
 * 保存新增角色信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const addRole = (params: any) => {
  return http.post('system/role/add', params);
}

/**
 * 导出角色信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const exportRole = (params?: any) => {
  return http.get('system/role/export', {params});
}


/**
 * 验证角色名称是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkRoleNameUnique = (params: any) => {
  params.loading = false;
  return http.post('system/role/checkRoleNameUnique', params);
}


/**
 * 验证角色权限是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkRoleKeyUnique = (params: any) => {
  params.loading = false;
  return http.post('system/role/checkRoleKeyUnique', params);
}

