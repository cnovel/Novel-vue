import http from '@/utils/http';


/**
 * 获取调度任务列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getJobList = (params?: any) => {
  return http.get('monitor/job/list', {params});
}


/**
 * 删除任务调度信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const removeJob = (params: any) => {
  return http.delete('monitor/job/remove', {params});
}


/**
 * 保存任务调度信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const addJob = (params: any) => {
  return http.post('monitor/job/add', params);
}


/**
 * 保存修改用户信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const updateJob = (params: any) => {
  return http.put('monitor/job/edit', params);
}


/**
 * 任务调度状态修改
 * @param params
 * @returns {*|Promise<any>}
 */
export const changeStatusJob = (params: any) => {
  return http.put('monitor/job/changeStatus', params);
}


/**
 * 任务调度立即执行一次
 * @param params
 * @returns {*|Promise<any>}
 */
export const runJob = (params: any) => {
  return http.post('monitor/job/run', params);
}

/**
 * 校验cron表达式是否有效
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkCronExpressionIsValid = (params: any) => {
  params.loading = false;
  return http.get('monitor/job/checkCronExpressionIsValid', {params});
}

/**
 * 导出任务调度信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const exportJob = (params?: any) => {
  if (!params) {
    params = {};
  }
  return http.get('monitor/job/export', {params});
}
