import http from '@/utils/http';

/**
 * 获取系统用户操作日志列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getLogsList = (params?: any) => {
  return http.get('system/logs/list', {params});
}

/**
 * 清空所有操作日志
 * @returns {*|Promise<any>}
 */
export const cleanLogs = () => {
  return http.delete('system/logs/clean');
}

/**
 * 删除操作日志
 * @param params
 * @returns {*|Promise<any>}
 */
export const removeLogs = (params: any) => {
  return http.delete('system/logs/remove', {params});
}

/**
 * 导出操作日志
 * @param params
 * @returns {*|Promise<any>}
 */
export const exportLogs = (params?: any) => {
  if (!params) {
    params = {};
  }
  return http.get('system/logs/export', {params});
}
