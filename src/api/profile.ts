import http from '@/utils/http';

/**
 * 获取系统用户列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getUserInfo = (params?: any) => {
  return http.get('system/user/getUserInfo', {params});
}

/**
 * 更新当前登录用户个人信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const updateUserInfo = (params: any) => {
  return http.put('system/user/update', params);
}

/**
 * 更新当前登录用户密码
 * @param params
 * @returns {*|Promise<any>}
 */
export const modifyPassword = (params: any) => {
  return http.put('system/user/modifyPassword', params);
}
