import http from '@/utils/http';

/**
 * 获取登录用户信息
 * @returns {*|Promise<any>}
 */
export const getLoginUserInfo = () => {
  return http.get('/system/user/getLoginUserInfo');
}

/**
 * 更新当前登录用户密码
 * @param params
 * @returns {*|Promise<any>}
 */
export const resetPwd = (params: any) => {
  return http.put('system/user/resetPwd/' + params.id);
}

/**
 * 获取系统用户列表
 * @param params
 * @returns {*|Promise<any>}
 */
export const getUserList = (params?: any) => {
  return http.get('system/user/list', {params});
}

/**
 * 获取用户角色及系统所有角色
 * @param params
 * @returns {*|Promise<any>}
 */
export const getUserPost = (params?: any) => {
  return http.get('system/user/getUserPost', {params});
}

/**
 * 获取用户角色及系统所有角色
 * @param params
 * @returns {*|Promise<any>}
 */
export const getUserRole = (params?: any) => {
  return http.get('system/user/getUserRole', {params});
}

/**
 * 删除用户信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const removeUser = (params: any) => {
  return http.delete('system/user/remove', {params});
}


/**
 * 保存新增用户信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const addUser = (params: any) => {
  return http.post('system/user/add', params);
}


/**
 * 保存修改用户信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const updateUser = (params: any) => {
  return http.put('system/user/edit', params);
}


/**
 * 获取用户角色及系统所有角色
 * @param params
 * @returns {*|Promise<any>}
 */
export const getPostListByUserId = (params: any) => {
  return http.get('system/user/getPostListByUserId/' + params.id);
}


/**
 * 导出用户信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const exportUser = (params?: any) => {
  if (!params) {
    params = {};
  }
  return http.get('system/user/export', {params});
}

/**
 * 下载用户导入模板
 * @returns {*|Promise<any>}
 */
export const downloadUserImportTemplate = () => {
  return http.get('system/user/importTemplate');
}


/**
 * 验证用户名是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkUserNameUnique = (params: any) => {
  params.loading = false;
  return http.post('system/user/checkUserNameUnique', params);
}


/**
 * 验证用户手机号是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkPhoneUnique = (params: any) => {
  params.loading = false;
  return http.post('system/user/checkPhoneUnique', params);
}

/**
 * 验证用户邮箱是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkEmailUnique = (params: any) => {
  params.loading = false;
  return http.post('system/user/checkEmailUnique', params);
}

