import http from '@/utils/http';

/**
 * 获取系统权限树
 * @param params
 * @returns {*|Promise<any>}
 */
export const getMenuTreeData = (params: any = {}) => {
  return http.get('system/menu/menuTreeTableData', {params});
}

/**
 * 获取菜单树
 * @param params
 * @returns {*|Promise<any>}
 */
export const getMenuTree = (params?: any) => {
  return http.get('system/menu/menuTreeData', {params});
}

/**
 * 获取菜单树,不含按钮
 * @param params
 * @returns {*|Promise<any>}
 */
export const menuTreeSelectData = (params?: any) => {
  return http.get('system/menu/menuTreeSelectData', {params});
}

/**
 * 保存新增菜单信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const addMenu = (params: any) => {
  return http.post('system/menu/add', params);
}

/**
 * 保存编辑菜单信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const updateMenu = (params: any) => {
  return http.put('system/menu/edit', params);
}

/**
 * 删除菜单信息
 * @param params
 * @returns {*|Promise<any>}
 */
export const removeMenu = (params: any) => {
  return http.delete('system/menu/remove', {params});
}

/**
 * 验证菜单名称是否唯一
 * @param params
 * @returns {*|Promise<any>}
 */
export const checkMenuNameUnique = (params: any) => {
  params.loading = false;
  return http.post('system/menu/checkMenuNameUnique', params);
}

