import http from '@/utils/http';

/**
 * 基本信息
 * @returns {*|Promise<any>}
 */
export const basic = () => {
  const params = {loading: true}
  return http.get('/system/druidMonitor/basic', {params});
}

/**
 * 重置所有
 * @returns {*|Promise<any>}
 */
export const resetAll = () => {
  const params = {loading: true}
  return http.put('/system/druidMonitor/resetAll', {}, {params});
}

/**
 * 记录并重置所有
 * @returns {*|Promise<any>}
 */
export const logAndReset = () => {
  const params = {loading: true}
  return http.put('/system/druidMonitor/logAndReset', {}, {params});
}

/**
 * 数据源信息
 * @returns {*|Promise<any>}
 */
export const datasource = () => {
  const params = {loading: true}
  return http.get('/system/druidMonitor/datasource', {params});
}

/**
 * 激活连接的堆栈信息
 * @returns {*|Promise<any>}
 */
export const activeConnectionStackTrace = () => {
  const params = {loading: true}
  return http.get('/system/druidMonitor/activeConnectionStackTrace', {params});
}

/**
 * 数据源信息
 * @returns {*|Promise<any>}
 */
export const datasourceById = (id) => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/datasource/${id}`, {params});
}

/**
 * 数据源信息连接信息
 * @returns {*|Promise<any>}
 */
export const connectionInfo = (id) => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/connectionInfo/${id}`, {params});
}

/**
 * 激活连接的堆栈信息
 * @returns {*|Promise<any>}
 */
export const activeConnectionStackTraceById = (id) => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/activeConnectionStackTrace/${id}`, {params});
}

/**
 * sql监控
 * @returns {*|Promise<any>}
 */
export const sql = (id) => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/sql?dataSourceId=${id}`, {params});
}

/**
 * sql Detail
 * @returns {*|Promise<any>}
 */
export const sqlDetail = (id) => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/sqlDetail/${id}`, {params});
}

/**
 * sql监控 wall
 * @returns {*|Promise<any>}
 */
export const wall = () => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/wall`, {params});
}

/**
 * sql监控 wall
 * @returns {*|Promise<any>}
 */
export const wallById = (id) => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/wall/${id}`, {params});
}

/**
 * webUri监控
 * @returns {*|Promise<any>}
 */
export const webUri = () => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/webUri`, {params});
}

/**
 * webUri监控
 * @returns {*|Promise<any>}
 */
export const webUriByUrl = (url) => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/webUriByUrl?url=${url}`, {params});
}

/**
 * webApp监控
 * @returns {*|Promise<any>}
 */
export const webApp = () => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/webApp`, {params});
}

/**
 * spring监控
 * @returns {*|Promise<any>}
 */
export const spring = () => {
  const params = {loading: true}
  return http.get(`/system/druidMonitor/spring`, {params});
}

/**
 * spring Detail
 * @returns {*|Promise<any>}
 */
export const springDetail = (params) => {
  params.loading = true;
  return http.get(`/system/druidMonitor/springDetail`, {params});
}
