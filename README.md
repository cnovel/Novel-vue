#### 介绍文档 | [部署文档](doc/BUILD.md) 

![vue](https://badgen.net/badge/vue/2.7.14/green)
![vuex](https://badgen.net/badge/vuex/3.6.2/blue)
![element-ui](https://badgen.net/badge/element-ui/2.15.13/cyan)
![axios](https://badgen.net/badge/axios/0.27.2/yellow)
![sass](https://badgen.net/badge/sass/1.56.1/orange)
![vue@cli](https://badgen.net/badge/vue@cli/5.0.4/pink)
[![star](https://gitee.com/cnovel/Novel-vue/badge/star.svg?theme=dark)](https://gitee.com/cnovel/Novel-vue/stargazers)

# Novel简介
一直想做一款后台管理系统，看了很多优秀的开源项目，从中发现了若依开源框架，从她出现以来就一直关注，但发现其中的功能太过强大，部分功能也不太适合自己，并且自己也一直想要动手学习一下若依的强大之处，便有了自己现在的novel。

它可以用于所有的Web应用程序，如网站管理后台，网站会员中心，CMS，CRM，OA等等，当然，您也可以对她进行深度定制，以做出更强系统。所有前端后台代码封装过后十分精简易上手，出错概率低。同时支持移动客户端访问。系统会陆续更新一些实用功能。

![阿里云](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241053327.jpeg)
> 阿里云服务器抢购：服务器0元试用，首购低至0.9元/月起 [点我进入](https://www.aliyun.com/daily-act/ecs/activity_selection?userCode=imu9ntnh)

![腾讯云](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241055658.jpeg)

>【腾讯云】多款云产品1折起，买云服务器送免费机器，最长免费续3个月 [点我进入](https://curl.qcloud.com/pJpTU9sk)

>【腾讯云】推广者专属福利，新客户无门槛领取总价值高达2860元代金券，每种代金券限量500张，先到先得。 [点我进入](https://curl.qcloud.com/6xVKUFiE)

# 内置功能


1. 用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2. 部门管理：配置系统组织机构（公司、部门、小组），树结构展现。
3. 岗位管理：配置系统用户所属担任职务。
4. 菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5. 角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6. 操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
7. 登录日志：系统登录日志记录查询包含登录异常。
8. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
9. 在线用户：当前系统中活跃用户状态监控。
10. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
11. 数据源监控：监视当前系统数据库连接池状态。
12. SQL监控：可查看SQL执行信息。
13. WEB监控：可分析各种请求统计信息。
14. URL监控：可分析出各个接口请求信息。
15. Spring监控：可分析各个方法执行信息。
16. SQL防火墙：可查看SQL执行过程的一些统计信息。
17. 代码生成：前后端代码的生成支持CRUD下载。
18. 参数管理：对系统动态配置常用参数。
19. 个人网盘：网盘存储功能（可选）。

# 在线体验

前端项目地址：[Novel-vue](https://gitee.com/cnovel/Novel-vue)

后端项目地址：[Novel-api](https://gitee.com/cnovel/Novel-api)

网盘后端项目地址：[disk-api](https://gitee.com/cnovel/disk-api)

网盘前端项目地址：[disk-vue](https://gitee.com/cnovel/disk-vue)

后端接口文档地址：[Novel-api](http://doc.cnovel.top)

演示地址：[https://cnovel.top](https://cnovel.top)


# 演示图

| ![用户登录](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241055743.png "用户登录")         | ![系统首页](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241055484.png "系统首页")             |
|-----------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------|
| ![用户管理](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241055932.png "用户管理")         | ![用户编辑](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241055644.png "用户编辑")             |
| ![角色管理](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241055985.png "角色管理")         | ![角色编辑](https://raw.githubusercontent.com/lizhen789/pic/main/test/202308241056529.png "角色编辑")     |
| ![菜单管理](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241116698.png "菜单管理")         | ![菜单编辑](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241056802.png "菜单编辑")             |
| ![岗位管理](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241056046.png "岗位管理")         | ![岗位编辑](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241114800.png "岗位编辑")             |
| ![部门管理](https://raw.githubusercontent.com/lizhen789/pic/main/test/202308241116748.png "部门管理") | ![部门编辑](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241056406.png "部门编辑")             |
| ![参数管理](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241056163.png "参数管理")         | ![参数编辑](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241056066.png "参数编辑")             |
| ![操作日志](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241056139.png "操作日志")         | ![日志详情](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241056804.png "日志详情")             |
| ![登录日志](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241121203.png "登录日志")         | ![服务监控](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241057672.png "服务监控")             |
| ![在线用户](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241057712.png "在线用户")         | ![数据源监控](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241057120.png "数据源监控")           |
| ![个人信息](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241057687.png "个人信息")         | ![编辑头像](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241057156.png "编辑头像")             |
| ![定时任务](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241059717.png "定时任务")         | ![任务日志](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241057034.png "任务日志")             |
| ![SQL监控](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241057166.png "SQL监控")       | ![SQL监控详情](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241058040.png "SQL监控详情")       |
| ![WEB监控](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241058339.png "WEB监控")       | ![SQL防火墙](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241101277.png "SQL防火墙")         |
| ![Spring监控](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241101320.png "Spring监控") | ![Spring监控详情](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241119563.png "Spring监控详情") |
| ![个人网盘](https://raw.githubusercontent.com/lizhen789/pic/main/test/202308241058295.png "个人网盘") | ![图片预览](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241058018.png "图片预览")             |

# 捐赠支持

开源项目不易，若此项目能得到你的青睐，可以捐赠支持作者持续开发与维护。
![收款码](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241058307.jpeg)

感谢打赏 [new fuel]() ,[1024545866](https://gitee.com/lijiajia) ,[1*1]() ,[*梦婷]() ,[*生]() ,[*田]()

**另：**

本人还有一个开源项目，是该项目中菜单管理中使用的图标组件，喜欢的可以去看看，帮忙点个`star`  [e-icon-picker](https://gitee.com/cnovel/e-icon-picker)
,非常感谢！

**演示图**

![示例图片](https://raw.gitmirror.com/lizhen789/pic/main/test/202308241058146.jpeg "示例图片")
